# A tiny compiler bench

For build Go binary, you have 3 famous compiler. **Official go** compiler, **GCC-GO** from GNU project and **TinyGo** based on LLVM (and intended for use in "small places")    

**This bench try to show 3 point**:
- **Faster** to build
- **Smallest** binary
- Binary **performaces**

## Lets **Go** ...
![Gopher image](doc/media/fiveyears.jpg)
*Gopher image by [Renee French][rf], licensed under [Creative Commons 3.0 Attributions license][cc3-by].*

### Bench table
| Type          | Info                                                                              |
|---------------|-----------------------------------------------------------------------------------|
| Uname         | Linux Arrakis 5.4.72-1-lts #1 SMP Sat, 17 Oct 2020 13:30:57 +0000 x86_64 GNU/Linux|
| CPU model     | Intel(R) Core(TM) i7-8559U CPU @ 2.70GHz                                          |
| Memory info   | 32GiB 2400MHz                                                                     |
| Storage drive | NVMe Samsung SSD 970 PRO 512GB                                                    |

### Compiler

*For GCC-GO build, i replace my Go install by GCC-GO*

| Name        | Version                     |
|-------------|-----------------------------|
| GCC-GO      | go1.14.4 gccgo (GCC) 10.2.0 |
| TinyGo      | 0.15.0                      |
| Official Go | go1.15.4                    |

### Results

*ST is for Statically linked*   
*O2 is for optimisation level*

Bench test are run with `perf stat` *Performance analysis tools for Linux* hi is built in to linux kernel tree, so this is probably a good tool to bench ...   
Build Bench are manualy runing, result can be found on `bench_build.txt`   
Bench is runing by `bench.sh` and result are redirected to `bench_run.txt`   
You can find all RAW result on [/doc/result](doc/result/)   

#### Build time / size
| Name              | Size    |
|-------------------|---------|
| bench_go          | 2.1M    |
| bench_gccgo       | 66K     |
| bench_gccgo_STO2  | 6.8M    |
| bench_tinygo      | 361K    |

| Compiler    | CMD                                      | Time                  | Status |
|-------------|------------------------------------------|-----------------------|:------:|
| Official Go | go build -o bench_go -a                  |   1.568412597 sec     | ✅      |
| GCC-GO      | gccgo main.go -o bench_gccgo             |   0.170248385 sec     | ✅      |
| GCC-GO STO2 | gccgo -static -O2 main.go -o bench_gccgo |   1.034481239 sec     | ✅      |
| TinyGo      | tinygo build -o bench_tinygo             | 101.191178031 sec     | ✅      |

#### Local run time
For this part, whe see 'internal bench' for checking 4 hard cases, all case are tested multiple times.   

Time counting is done with this code:
```
type intime struct {
	start    time.Time
	stop     time.Time
	duration time.Duration
}

func (i *intime) Start() {
	i.start = time.Now()
}

func (i *intime) Stop() {
	i.stop = time.Now()
	i.duration = i.stop.Sub(i.start)
}
```
##### [Math] Prime number search

| Compiler    | Time             | Status |
|-------------|------------------|:------:|
| Official Go |   509.82006  ms  | ✅      |
| GCC-GO      |   632.843018 ms  | ✅      |
| GCC-GO STO2 | 1.006531426 sec  | ❌      |
| TinyGo      |   399.317119 ms  | ✅      |

##### [Map] A very big map

| Compiler    | Time                    | Status |
|-------------|-------------------------|:------:|
| Official Go |           303.527414 ms | ✅      |
| GCC-GO      |           406.668759 ms | ✅      |
| GCC-GO STO2 |           444.171902 ms | ✅      |
| TinyGo      | **2h 42m 55.416901711 sec** | ❌      |

##### [Str] string concatenation

| Compiler    | Time                | Status |
|-------------|---------------------|:------:|
| Official Go |     4.221176024 sec | ✅      |
| GCC-GO      |     2.944123142 sec | ✅      |
| GCC-GO STO2 |     7.969823537 sec | ✅      |
| TinyGo      | **3m 29.684675044 sec** | ❌      |

##### [Loop] Loop managing

| Compiler    | Time             | Status |
|-------------|------------------|:------:|
| Official Go | 3.519736744  sec | ❌      |
| GCC-GO      | 9.281379494  sec | ❌      |
| GCC-GO STO2 |          245 ns  | ✅      |
| TinyGo      |   457.010533 ms  | ✅      |

#### Total execution time

| Compiler    | Time               | Status |
|-------------|--------------------|:------:|
| **Official Go** |    8.562514977 sec | ✅      |
| GCC-GO      |   13.320942949 sec | ✅      |
| GCC-GO STO2 |    9.436250825 sec | ✅      |
| ~~TinyGo~~      | 9986.216965017 ms  | ❌      |