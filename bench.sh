#!/bin/bash
echo "=============== OFFICIAL GO ===============" > doc/result/bench_run.txt
perf stat ./bin/bench_go 1>>doc/result/bench_run.txt 2>>doc/result/bench_run.txt
echo "" >> doc/result/bench_run.txt

echo "=============== GCC GO ===============" >> doc/result/bench_run.txt
perf stat ./bin/bench_gccgo 1>>doc/result/bench_run.txt 2>>doc/result/bench_run.txt
echo "" >> doc/result/bench_run.txt

echo "=============== GCC GO STO2 ===============" >> doc/result/bench_run.txt
perf stat ./bin/bench_gccgo_STO2 1>>doc/result/bench_run.txt 2>>doc/result/bench_run.txt
echo "" >> doc/result/bench_run.txt

echo "=============== TINY GO ===============" >> doc/result/bench_run.txt
perf stat ./bin/bench_tinygo 1>>doc/result/bench_run.txt 2>>doc/result/bench_run.txt
echo "" >> doc/result/bench_run.txt
